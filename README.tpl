[self]: https://gitlab.com/konnorandrews/{{crate}}
[`update`]: https://docs.rs/{{crate}}/{{version}}/deep_struct_update/macro.update.html

[![Version](https://img.shields.io/static/v1?label=version&message={{version}}&color=informational)]()
[![Crates.io](https://img.shields.io/crates/v/{{crate}})](https://crates.io/crates/{{crate}})
[![docs.rs](https://img.shields.io/docsrs/{{crate}})](https://docs.rs/{{crate}}/{{version}}/deep_struct_update/)
[![Crates.io](https://img.shields.io/crates/l/{{crate}})](#license)

# `::{{crate}}`

{{readme}}

<br>

#### License

<sup>
Licensed under either of <a href="LICENSE-APACHE">Apache License, Version
2.0</a> or <a href="LICENSE-MIT">MIT license</a> at your option.
</sup>

<br>

<sub>
Unless you explicitly state otherwise, any contribution intentionally submitted
for inclusion in {{crate}} by you, as defined in the Apache-2.0 license, shall be
dual licensed as above, without any additional terms or conditions.
</sub>
