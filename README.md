[self]: https://gitlab.com/konnorandrews/deep-struct-update
[`update`]: https://docs.rs/deep-struct-update/0.1.0/deep_struct_update/macro.update.html

[![Version](https://img.shields.io/static/v1?label=version&message=0.1.0&color=informational)]()
[![Crates.io](https://img.shields.io/crates/v/deep-struct-update)](https://crates.io/crates/deep-struct-update)
[![docs.rs](https://img.shields.io/docsrs/deep-struct-update)](https://docs.rs/deep-struct-update/0.1.0/deep_struct_update/)
[![Crates.io](https://img.shields.io/crates/l/deep-struct-update)](#license)

# `::deep-struct-update`

[Struct update syntax](https://doc.rust-lang.org/book/ch05-01-defining-structs.html#creating-instances-from-other-instances-with-struct-update-syntax) with support for nested structs.

```rust
use deep_struct_update::update;

struct Person {
    name: String,
    job: Job,
    age: u8
}

struct Job {
    position: String,
    company: String
}

let bob = Person {
    name: String::from("Bob"),
    job: Job {
        position: String::from("Programmer"),
        company: String::from("Evil Inc.")
    },
    age: 29
};

let alice = update! {
    name: String::from("Alice"),
    age: 31,
    job: {
        position: String::from("Senior Programmer")
    }
    ..bob
};

assert_eq!(alice.age, 31);
assert_eq!(alice.name, "Alice");
assert_eq!(alice.job.position, "Senior Programmer");
assert_eq!(alice.job.company, "Evil Inc.");
```

<br>

#### License

<sup>
Licensed under either of <a href="LICENSE-APACHE">Apache License, Version
2.0</a> or <a href="LICENSE-MIT">MIT license</a> at your option.
</sup>

<br>

<sub>
Unless you explicitly state otherwise, any contribution intentionally submitted
for inclusion in deep-struct-update by you, as defined in the Apache-2.0 license, shall be
dual licensed as above, without any additional terms or conditions.
</sub>
